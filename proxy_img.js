const myImage = (function () {
    const imgNode = document.createElement('img')
    document.body.appendChild(imgNode)
    return {
        setSrc: function (src) {
            imgNode.src = src
        }
    }
})()

const proxyImg = (function () {
    const img = new Image()
    img.onload = function () {
        myImage.setSrc(this.src)
    }
    return {
        setSrc: function (src) {
            myImage.setSrc('')
            img.src = src
        }
    }
})()

proxyImg.setSrc('')

/**
 * 单一职责原则，就一个类（对象，函数）而言，应该仅有一个引起它变化的原因；
 * 如果一个对象承担了多项职责，就意味着这个对象将变得巨大，引起它变化的原因可能是多个的，
 * 这种设计可能会遭到意外的破坏；
 * **/ 