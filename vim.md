## 快捷键 
  ### 操作类快捷键
  | 操作名称  | 命令快捷键    |  
  | :----:    |  :----:       |
  |   复制    |   y Y （Y 复制行）|
  |   粘贴    |   p P (P在光标前粘贴) |

  ### 命令操作
  :w       保存文件
  :e XXX文件 打开某个文件
  :q 不保存退出
  :h 帮助
  :new 新建文件

  ### 搜索文件
  


![命令图片](./static/code.jpg)


## HTTP缓存机制
  HTTP缓存分为，强制缓存和协商缓存
  ### 强制缓存
   强制缓存： expires cache-control cache-control的优先级高于expires
   expires 是HTTP1.0时控制网页缓存的字段，时根据服务器返回该请求结果的缓存到期时间，即在此发起请求时，如果客户端的时间小于expires的值则直接使用缓存结果

   Cache-Control 在HTTP1.1中，时最重要的规则之一，主要用于控制页面缓存，取值如下：

   public   所有内容都被缓存（包括客户端、代理服务器、CDN等）
   private  只有客户端可以缓存，cache-control的默认值
   no-cache 客户端缓存内容，但是是否使用缓存则需要协商缓存来决定
   no-store 所有内容都不使用缓存，即不适用强制缓存也不适用协商缓存
   max-age= 缓存过期时间

   cache-control 和 expires同时存在时，只有cache-control生效

   ### 协商缓存
   协商缓存，就是在强制缓存失效后，浏览器携带缓存标识想服务器发起请求，服务器根据缓存标识，决定是否使用缓存的过程，主要有以下两种情况：
   1、协商缓存生效，返回304，服务器告诉浏览器资源未更新，客户端则从缓存中直接获取资源；
   2、协商缓存失败，返回200和请求结果；

   协商缓存的标识，也是在相应报文的HTTP头和请求结果中一起返回给浏览器的，控制协商缓存的字段有：

   Last-Modified/If-Modified-Since  成对出现

   Etag/If-None-Match               成对出现，

   其中Etag/If-None-Match 优先级别高于Last-Modified/If-Modified-Since
   

  