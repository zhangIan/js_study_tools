const strategies = {
    isNonEmpty: (val, msg) => {
        if (val === '') {
            return msg
        }
    },
    minLength: (val, len, msg) => {
        if (val.length < len) {
            return msg
        }
    },
    isMobile: (val, msg) => {
        if (!/^1[3|4|5|6|7|9][0-9]${9}$/.test(val)) {
            return msg
        }
    }
}

class Validator {
    constructor() {
        this.cache = []
    }

    add (val, rule, errorMsg) {
        const ary = rule.split(':')
        this.cache.push(() => {
            const strategy = ary.shift()
            ary.unshift(val)
            ary.push(errorMsg)
            return strategies[strategy].apply(val, ary)
        })
    }

    start () {
        for (let i = 0, validatorFunc; validatorFunc = this.cache[i ++];) {
            const msg = validatorFunc()
            if (msg) {
                return msg
            }
        }
    }
}