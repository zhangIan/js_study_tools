public abstract class Animal {
    abstract void makeSound();
}

public class Duck extends Animal {
    public void makeSound() {
        System.out.println("hhh");
    }
}

public class Chicken extends Animal {
    public void makeSound() {
        System.out.println("hahah");
    }
}

public class AnimalSound {
    public void makeSound(Animal animal) {
        animal.makeSound();
    }
}

public class Test {
    public static void main(String args[]) {
        AnimalSound animalSound = new AnimalSound();

        Animal duck = new Duck();
        Animal chicken = new Chicken();
        animalSound.makeSound(duck);
        animalSound.makeSound(chicken);
    }
}