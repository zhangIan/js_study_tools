const Flower = function () {}

const xiaoming = {
    sendFlower: (target) => {
        const flower = new Flower()
        target.receiveFlower(flower)
    }
}

const proxyB = {
    receiveFlower: (flower) => {
        targetA.listenModel(() => {targetA.receiveFlower(flower)})
    }
}

const targetA = {
    receiveFlower: (flower) => console.log('receiveFLower: ', flower),
    listenModel: (fn) => {setTimeout(() => {
        fn()
    }, 1000);}
}

xiaoming.sendFlower(proxyB)

/* 
    ** 代理模式包含 客户（xiaoming）， 代理（proxyB）， 目标（targetA）
    客户调用 代理的方法（receiveFlower） ==> 代理调用目标的方法 (listenModel)

    代理，可以过滤掉一些目标，不希望接受的请求和访问，这种代理叫做保护代理

    把一些开销很大的对象，延迟到真正需要它的时候在创建，可以使用虚拟代理
*/