const miniConsole = (() => {
    const cache = []

    const handler = e => {
        if (e.keyCode === 113 || e.keycode === 113) {
            const script = document.createElement('script')
            script.onload = function () {
                for (const fn of cache) {
                    fn()
                } 
            }

            script.src = 'miniConsole.js'
            document.getElementsByTagName('head')[0].appendChild(script)
            document.body.removeEventListener('keydown', handler)
        }
    }

    document.body.addEventListener('keydown', handler)

    return {
        log: function () {
            const args = arguments
            cache.push(function () {
                return miniConsole.log.apply(miniConsole, args)
            })
        }
    }
})()


const mult = function () {
    console.log('mult start')
    let a = 1
    for (let i =0, l = arguments.length; i < l; i++) {
        a = a * arguments[i]
    }

    return a
}

const proxyMult = (function () {
    const cache = {}
    return function () {
        const args = Array.prototype.join.call(arguments, ',')

        if (args in cache) {
            console.log(args)
            return cache[args]
        }
        
        cache[args] = mult.apply(this, arguments)
        return cache
    }
})()

const res = proxyMult(1, 2, 3, 4)
console.log(res)

const plus = function () {
    let a = 0
    for (let i = 0, l = arguments.length; i < l; i++) {
        a = a + arguments[i]
    }
    return a
}

const createProxyFactory = function (fn) {
    const cache = {}

    return function () {
        const args = Array.prototype.join.call(arguments, ',')
        if (args in cache) {
            return cache[args]
        }
        cache[args] = fn.apply(this, arguments)
        return cache
    }
}

const proxyPlus = createProxyFactory(plus)

const sumPlus = proxyPlus(1, 2, 4, 5)

console.log(sumPlus)